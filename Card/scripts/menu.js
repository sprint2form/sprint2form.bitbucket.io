// getElementById
function $id(id) {
    return document.getElementById(id);
}

// To increase cart item
function increaseCartItem(buttonID) {

    document.getElementById(buttonID).value = parseInt(document.getElementById(buttonID).value) + 1;

}

// To decrease cart item
function decreaseCartItem(buttonID) {
    if (parseInt(document.getElementById(buttonID).value) >= 1)
        document.getElementById(buttonID).value = parseInt(document.getElementById(buttonID).value) - 1;

}