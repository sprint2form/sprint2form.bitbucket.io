//Function to validate the format for phone number and triggers warning to add country code first
function phonenumber(parametertext)
{
  var phoneNumber = /^[1-9]{1}[0-9]*$/;
  if(parametertext.value.match(phoneNumber))
  {
      return true;
  }
  else
  {
     alert("Please enter country code first");
     document.getElementById("contactForm").addEventListener("click", function (event) {
        event.preventDefault()
    });
  }
}

//Function to validate form's fields
function validateForm()
{
    
    //Declaring variables
    var fname = document.getElementById("name").value;
    var phoneNo = document.getElementById("mobile").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var address = document.getElementById("address").value;

    //Name validation
    if (fname == undefined || fname == "" || fname == null) {
        alert("Please enter your name");
        document.getElementById("contactForm").addEventListener("click", function (event) {
            event.preventDefault()
        });
    }
    //Address validation
    if (address == undefined || address == "" || address == null) {
        alert("Please enter your Mobile No");
        document.getElementById("contactForm").addEventListener("click", function (event) {
            event.preventDefault()
        });
    }
    //Phone number validation
    if (phoneNo == undefined || phoneNo == "" || phoneNo == null) {
        alert("Please enter your Mobile No");
        document.getElementById("contactForm").addEventListener("click", function (event) {
            event.preventDefault()
        });
    }
    //Email validation
    if (email == undefined || email == "" || email == null) {
        alert("Please enter your email id");
        document.getElementById("contactForm").addEventListener("click", function (event) {
            event.preventDefault()
        });
    }
    //Password validation
    if (password == undefined || password == "" || password == null) {
        alert("Please enter your email id");
        document.getElementById("contactForm").addEventListener("click", function (event) {
            event.preventDefault()
        });
    }
    //Calling the phone number validation function
    phonenumber(document.contactForm.mobile);
    document.getElementById("contactForm").addEventListener("click", function (event) {
        event.preventDefault()
    });
}

//checking if birthdate year is less than 13 or greater than 50
function checkBirthday()
{
    var birthDate = document.getElementById('birthDate').value;
    var today = new Date();
    var diff = (today.getTime() - new Date(birthDate).getTime()) / 1000;
    diff /= (60 * 60 * 24);
    diff = Math.abs(Math.round(diff / 365.25));
    if (diff < 13) {
        alert("You should be greater than 12 years");
        document.getElementById('birthDate').value = ""
    }
    else if (diff > 50) {
        alert("Do you need any help?");
    }
}

//Calling Functiontions to make submit button more interactive by changing colors when mouse is on submit button
document.getElementById("submit-btn").onmouseover = function () { mouseOver() };
document.getElementById("submit-btn").onmouseout = function () { mouseOut() };

//function to change color when mouse is over
function mouseOver() {
    document.getElementById("submit-btn").style.color = "black";
    document.getElementById("submit-btn").style.background = "#fff";
}

//function to change back the color when mouse is out
function mouseOut() {
    document.getElementById("submit-btn").style.color = "#fff";
    document.getElementById("submit-btn").style.background = "#000";
}